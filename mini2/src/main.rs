use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use std::collections::HashMap;
use lazy_static::lazy_static;

/// Define a fruit:price map for lambda function to use
lazy_static! {
    static ref FRUITS_INFO: HashMap<String, f64> = {
        let mut map = HashMap::new();
        map.insert("apple".to_string(), 3.0);
        map.insert("orange".to_string(), 2.0);
        map.insert("pear".to_string(), 1.5);
        map
    };
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful information from the request
    let fruitlist = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("fruitlist"))
        .unwrap_or("");

    let fruits: Vec<&str> = fruitlist.split(",").collect();
    let mut total_price: f64 = 0.0; // variable to store total price of fruit inputs
    let mut message = format!("Hello, this is an AWS Lambda HTTP request project. \n");

    for fruit in fruits{
        println!("{}", fruit);
        if let Some(&value) = FRUITS_INFO.get(fruit) {
            println!("Value of {} is: {}", fruit, value);
            message.push_str(&format!("Value of {} is: {}.\n", fruit, value));
            total_price += value;
        }
    }
    
    message.push_str(&format!("Total value is: {}.\n", total_price));

    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(message.into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
