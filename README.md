# yz858-ids721-mini2

In mini-project 2, I created a AWS Lambda function using Rust to calculate the total price of products.

The Lambda function accesses the price list and uses this information to add up all the products.

API Gateway: https://m3w26xebbpfbm6zuuzxku2twcq0pxvkf.lambda-url.us-west-2.on.aws/

To input fruits, concatenate fruit name (appple, orange or pear) with ',' and supply it to a parameter called fruitlist. 

For example, https://m3w26xebbpfbm6zuuzxku2twcq0pxvkf.lambda-url.us-west-2.on.aws/?fruitlist=apple,pear,apple


## Requirements
- [x] Lambda Functionality: Rust Lambda Function using Cargo Lambda
- [x] Data Processing: Process and transform sample data
- [x] API Gateway integration
- [x] Documentation

## Steps 
(Reference: https://www.cargo-lambda.info/guide/getting-started.html)

1. Install **Cargo Lambda**
```
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
```

2. Create new project
```
cargo lambda new mini2 \
    && cd mini2
```

3. Test and run function
```
cargo lambda watch
curl "LINK"
```

4. Build function for deployment
```
cargo lambda build --release
```

5. Deploy on AWS Lambda
- Add required IAM policies to add to the user: AWSLambda_FullAccess, iam:CreateRole
- Run `cargo lambda deploy`

6. API Gateway
There are 2 options AWS supports to build serverless API backed with Lambda Functions: Lambda Function URLs and AWS API Gateway service. Detailed difference can be found [here](https://www.ac3.com.au/resources/a-comparison-between-amazon-api-gateway-and-lambda-function-urls).

Since Function URL satisfies current scope of project and works easier, I choose to go with Lambda Function URLs.

Several points to be aware of:
1. If declaring NONE auth type, lambda function can be invoked directly without signing in. 
2. When function URL is called, Lambda maps the request to an event object type to pass to the lambda function. The response is mapped to an HTTP response by the function to send back to client thruogh functional URL. See detail [here](https://docs.aws.amazon.com/lambda/latest/dg/urls-invocation.html#urls-invocation-basics)

Just to note here, another useful info source is [here](https://github.com/awslabs/aws-lambda-rust-runtime).

To test locally:
```
sam local start-api
curl -v "URL_LINK_GIVEN"
```


## Screenshots
1. Fruit prices
![Prices](screenshots/Fruitprices.png)

2. Lambda deployed on AWS
![Deploy](screenshots/LambdaDeployed.png)

3. Successful Testing
![Test](screenshots/SuccessTest.png)

4. Add API Gateway to the Lambda Function
![API](screenshots/CallAPI.png)